const express = require("express");
const EventEmitter = require("events");

const { getData } = require("../utils/database");
const companyClient = require("../utils/companyClient");

const router = express.Router();
const messagesBus = new EventEmitter();

router.get("/webhookUrl", async (req, res) => {
	res.status(200).send("Hello!");
});

router.get("/sign", async (req, res) => {
	const { message, webhookUrl = "" } = req.query;

	if (getData(message)) {
		res.status(200).json({ signature: getData(message) });
		return;
	}

	res.status(202).json({ message: "Request queued for processing" });


	const requestObject = { message, webhookUrl};
	messagesBus.emit("pushmessage", requestObject);

	console.log(`> INFO: Message queued for processing -  ${message} -  ${new Date().toLocaleString()}`);
});

router.get("/verify", async (req, res) => {
	const { message, signature } = req.query;

	if (!message.length || !signature.length) {
		res.status(400).json({ error: "invalid_params", message: "Invalid parameters" });
		return;
	}

	try {
		const response = await companyClient.get("/crypto/verify", {
			params: {
				message,
				signature
			}
		});

		console.log(`> INFO: Signature verified as valid -  ${message} -  ${new Date().toLocaleString()}`);

		res.status(200).json({ message: response.data });
	} catch(e) {
		res.status(500).send("Internal server error");
	}
});

module.exports = { router,   messagesBus };