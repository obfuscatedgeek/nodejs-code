const express = require("express");
const app = express();
const { router  } = require("./routes/crypto");
const { cronJob } = require("./utils/cronjob");

app.use(express.json());

app.use("/crypto", router);

cronJob.start();

app.listen(3000, () => {
	console.log("Server listening on port 3000");
});