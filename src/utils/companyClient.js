const axios = require("axios");

const companyClient = axios.create({
	baseURL: "https://hiring.api.company.io/"
});


module.exports = companyClient;