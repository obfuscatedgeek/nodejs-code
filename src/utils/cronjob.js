const cron = require("node-cron");
const { RateLimiterMemory, RateLimiterRes } = require("rate-limiter-flexible");
const path = require("path");
const fs = require("fs");


const { getData, setData } = require("../utils/database");
const { postToClient } = require("../utils/postToClient");
const { messagesBus } = require("../routes/crypt");
const companyClient = require("./companyClient");

let messages = [];
const dataFilePath = path.join(__dirname, "../data.json");

if (fs.existsSync(dataFilePath)) {
	const fileData = fs.readFileSync(dataFilePath);

	if (fileData.length) {
		messages = JSON.parse(fileData);
	}
}

const rateLimiter = new RateLimiterMemory({
	points: 10,
	duration: 60,
});

messagesBus.on("pushmessage", message => {
	console.log(`> INFO: Message pushed event called - ${message} -  ${new Date().toLocaleString()}`);
	messages.push(message);
});

const cronJob = cron.schedule("*/35 * * * * *", async () => {
	console.log(`> INFO: Cron job started -  ${new Date().toLocaleString()}`);

	if (messages.length === 0) {
		console.log(`> INFO: No pending messages to sign -  ${new Date().toLocaleString()}`);
		fs.writeFileSync(dataFilePath, JSON.stringify(messages));
		return;
	}

	for (let i = 0; i < messages.length; i++) {
		try {
			const { message, webhookUrl } = messages[i];

			// check if data exists in database
			let response = getData(message);

			// call the company to get the response otherwise
			if (!response) {
				await rateLimiter.consume(1);

				console.log(`> INFO: Requesting signature from company -  ${message} -  ${new Date().toLocaleString()}`);

				response = await companyClient.get("/crypto/sign", {
					params: { message },
					timeout: 1000 * 2,
				});

				response = response.data;
				setData(message, response);

				console.log(`> INFO: Message signed -  ${message} - ${response} -  ${new Date().toLocaleString()}`);
			}

			postToClient(webhookUrl, response);
			messages.splice(i, 1);
		} catch (error) {
			if (error instanceof RateLimiterRes) {
				console.log(`> ERROR: Rate limit reached -  ${new Date().toLocaleString()}`);
				break;
			}

			console.log(`> ERROR: Failed company Request - ${error.response.data} -  ${new Date().toLocaleString()}`);
		}
	}
});

process.on("SIGTERM", () => {
	console.log(`> INFO: Shutdown called writing to file - ${messages.length} pending -  ${new Date().toLocaleString()}`);
	fs.writeFileSync(dataFilePath, JSON.stringify(messages));
	process.exit(0);
});

process.on("SIGINT", () => {
	console.log(`> INFO: Shutdown called writing to file - ${messages.length} pending - ${new Date().toLocaleString()}`);
	fs.writeFileSync(dataFilePath, JSON.stringify(messages));
	process.exit(0);
});


module.exports = { cronJob };