const database  = {};

function setData(key, value) {
	database[key] = value;
}

function getData(key) {
	return database[key];
}

module.exports = {
	setData,
	getData
};