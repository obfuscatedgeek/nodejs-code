const axios = require("axios");

const postToClient = async (webhookUrl, signature) => {
	if (!webhookUrl.length) {
		console.log(`> INFO: Posting to client stopped, empty URL -  ${new Date().toLocaleString()}`);
		return;
	}

	try {
		console.log(`> INFO: Message signed - ${signature} -  ${new Date().toLocaleString()}`);

		await axios.post(webhookUrl, {
			signature,
		});
	} catch (e) {
		console.log(`> ERROR: Posting to client failed - ${new Date().toLocaleString()}`);
	}
};


module.exports = {
	postToClient
};