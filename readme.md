## Problem
The details of the problem can be found [here](./problem.md)

The text with links saying "Point 1" - points to the problem number in the problem.md file

## Solution
The solution is done using NodeJS with ExpressJS.


## Installation
1. Clone this repository
2. Navigate into the project directory
3. Build the Docker image: `docker build -t Company-fullstack-nodejs .` [Point 4](./problem.md#we-would-like-the-following-functionality-from-your-api-problem4)

## Usage
To start the app in a Docker container, run the following command:
```docker
docker run -p 3000:3000 -d  Company-fullstack-nodejs
```

The app will be running inside the Docker container and accessible on http://localhost:3000.


## Endpoints
The following endpoints are exposed by the app:


### 1. `GET /crypto/sign` [Point 1](./problem.md#we-would-like-the-following-functionality-from-your-api-problem1)

The endpoint generates a cryptographically signature of the provided message.

### Parameters
- message - the message that needs to be encrypted
- webhookUrl - the URL which will be used to notify the user once their message has obtained the signature

### 2. `GET /crypto/verify`
The endpoint verifies if the provided message and the signature are valid
### Parameters
- message - the messaged that was signed
- signature - the signature obtained for the message

## Implementation details
The solution is implemented by breaking the problem into smaller chunks, namely

### 1. Message polling - incoming requests
Whenever a request comes in, we check our local in memory database to see if the message's signature already exists in the database, if exists, we immediately return with a status `200` and the signature of the message. [Point 2.1](./problem.md#we-would-like-the-following-functionality-from-your-api-problem2.1)

If the signature does not exist in our database we send a `202` response with message saying "Request queued for processing". [Point 2.2](./problem.md#we-would-like-the-following-functionality-from-your-api-problem2.2)

This way the use gets a response immediately.

On the background, we poll this message to be processed later.


### 2. Cron job
We have a cron job that runs every 35 seconds. It loops through our array of polled messages and calls the Company endpoint to receive the signature.

It picks a message and first checks if the signature already exists in the database, this can happen if multiple requests from different users contain the same message. We might have signed previously. If the signature is still not found we will request Company for the signature.

If the API call to Company is successful i.e. we received a signature, we notify the client using their `webhookUrl` that they had provided.  The signature is added to our database for any future messages and the request is removed from our messages pool

If the API call to Company fails, we keep the message in our pool and we will try again in the cron jobs next schedule.

With a rate limiter, we make sure that we are within the limit of 10 request per minute to the Company endpoint. [Point 3](./problem.md#we-would-like-the-following-functionality-from-your-api-problem3)


### 3. Persisting messages (Bonus) [Point 5](./problem.md#we-would-like-the-following-functionality-from-your-api-problem5)
The bonus task mentioned, if the service shutsdown and restarts, users who had previously requested a signature should be notified without re-requesting one from scratch.

This is achieved by listening to `SIGINT` or `SIGTERM` event in NodeJS. Depending on the method to terminate, these signal is often used to gracefully terminate a Node.js application.

On receiving the event, we write our messages pool to a file, there by persisting our pending requests. On restart of our service, we read again from this file and populate the our messages pool with the contents of this file. Once the service start, the cron job will start iterating through the messages pool and continue notifying the client on succesfully obtaining a signature.

## Custom Bonus
Additionally, in order to test this API with multiple requests but limit to the 10 requests per minute to the Company end point I have added a `npm script` which uses `artillery` to generate multiple requests (at present 10 requests) to kinda stress test the `/crypto/sign` endpoint. The script can be run using the following command

```nodejs
npm run stress-test
```

This will keep on making 10 requests to the endpoint each time it is ran.

Thanks for reading

Ejaz Bawasa.