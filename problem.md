For the challenge, we've implemented a simple HTTP API at https://hiring.api.company.io with the two following endpoints:

```bash
    GET /crypto/sign?message=<YOUR_MESSAGE>
    GET /crypto/verify?message=<YOUR_MESSAGE>&signature=<SIGNATURE>
```

The `/crypto/sign` endpoint will cryptographically sign the provided message with a secret RSA key that only we have access to, whereas the `/crypto/verify` endpoint will verify that the provided signature is a valid signature for the provided message.

To simulate an upstream service degradation scenario, we've made the `/crypto/sign` endpoint very unreliable. It will only succeed on the first try about half of the time, and when it fails, you may need to repeatedly try again for a few minutes before the request for a specific message will succeed. Degradation is message-specific, so, if a call with a specific message fails, it does not mean that a call with a different message will also fail.

The challenge is to build and serve a small API that re-exposes our unreliable endpoint in a more reliable way.

### We would like the following functionality from your API:
1. Expose a /crypto/sign endpoint with a similar input syntax to ours. <!-- ID: problem1 -->
2. Your endpoint must always return immediately (within ~2s), regardless of whether the call <!-- ID: problem2 -->to our endpoint succeeded.
    - If a result can be provided immediately, include it in the HTTP response with a 200 status code. <!-- ID: problem2.1 -->
    - If a result cannot be provided within the required timeframe, respond with a 202 status code, and notify the user with the result when it is ready. This should be implemented by allowing users of your API to specify a webhook notification URL for each request. <!-- ID: problem2.2 -->
3. You must not hit our endpoint more than 10 times per minute, but you should expect that your endpoint will get bursts of 60 requests in a minute, and still be able to eventually handle all of those. <!-- ID: problem2 -->
4. You must package your service in such a way that the service can be started as a Docker container or a set of containers with Docker Compose. This will help our engineers when they evaluate your challenge. We will not evaluate challenge solutions that are not containerised <!-- ID: problem4 -->
5. [Bonus] If your service shuts down and restarts, users who requested a signature before the shutdown should still be notified when their signature is ready without re-requesting one from scratch. <!-- ID: problem5 -->
